package com.singtel.app.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * The Class BookCatalog.
 */
@Entity
@Table(name = "book_catalog")
public class BookCatalog {

  /** The title. */
  @Column(name = "title")
  private String title;

  /** The isbn. */
  @Id
  @Column(name = "isbn", unique = true)
  private String isbn;

  /** The desc. */
  @Column(name = "description")
  private String desc;

  /**
   * Gets the title.
   *
   * @return the title
   */
  public String getTitle() {
    return title;
  }

  /**
   * Sets the title.
   *
   * @param title
   *          the new title
   */
  public void setTitle(String title) {
    this.title = title;
  }

  /**
   * Gets the isbn.
   *
   * @return the isbn
   */
  public String getIsbn() {
    return isbn;
  }

  /**
   * Sets the isbn.
   *
   * @param isbn
   *          the new isbn
   */
  public void setIsbn(String isbn) {
    this.isbn = isbn;
  }

  /**
   * Gets the desc.
   *
   * @return the desc
   */
  public String getDesc() {
    return desc;
  }
  /**
   * Sets the desc.
   *
   * @param isbn
   *          the new desc
   */
  public void setDesc(String desc) {
    this.desc = desc;
  }

  @Override
  public String toString() {
    return "BookCatalog [title=" + title + ", isbn=" + isbn + ", desc=" + desc + "]";
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#hashCode()
   */
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((isbn == null) ? 0 : isbn.hashCode());
    return result;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#equals(java.lang.Object)
   */
  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    BookCatalog other = (BookCatalog) obj;
    if (isbn == null) {
      if (other.isbn != null) {
        return false;
      }
    } else if (!isbn.equals(other.isbn)) {
      return false;
    }
    return true;
  }

}
