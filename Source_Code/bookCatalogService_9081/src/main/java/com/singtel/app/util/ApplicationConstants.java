package com.singtel.app.util;

/**
 * The Interface ApplicationConstants.
 */
public abstract interface ApplicationConstants {

  /** The Constant HOME_URL. */
  public static final String HOME_URL = "/";

  /** The Constant SERVICE_UP_MESSAGE. */
  public static final String SERVICE_UP_MESSAGE = "Book service  is Up on 9081";

  /** The Constant BOOK_SERVICE_URL. */
  public static final String BOOK_SERVICE_URL = "/api/book/";
  
  /** The Constant FIND_CATALOGS_URL. */
  public static final String FIND_CATALOGS_URL = "findAll";
  
  /** The Constant ADD_CATALOGS_URL. */
  public static final String ADD_CATALOGS_URL = "add";

  /** The Constant BOOK_NOT_FOUND. */
  public static final String BOOK_NOT_FOUND = "Book Catalog is not exist in our database";

  /** The Constant BOOK_UPDATE_MSG. */
  public static final String BOOK_UPDATE_MSG = "Book Catalog is updated Successfully";

  /** The Constant BOOK_SUCCESS_MSG. */
  public static final String BOOK_SUCCESS_MSG = "Book Catalog is added Successfully";
  
  /** The Constant BOOK_ISBN_FOUND. */
  public static final String BOOK_ISBN_FOUND = "Book Catalog of ISBN is already exist";

  /** The Constant UPDATE_CATALOGS_URL. */
  public static final String UPDATE_CATALOGS_URL = "update/{bookCode}";

  /** The Constant FIND_BY_CODE_URL. */
  public static final String FIND_BY_CODE_URL = "find/{bookCode}";

}
