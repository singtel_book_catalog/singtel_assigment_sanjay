package com.singtel.app.controller;

import com.singtel.app.model.BookCatalog;
import com.singtel.app.repository.BookCatalogRepository;
import com.singtel.app.util.ApplicationConstants;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * The Class BookCatalogController.
 */
@RestController
@RequestMapping(ApplicationConstants.BOOK_SERVICE_URL)
public class BookCatalogController {

  /** The book catalog repository. */
  @Autowired
  private BookCatalogRepository bookCatalogRepository;

  /**
   * Find all.
   *
   * @return the response entity
   */
  @GetMapping(ApplicationConstants.FIND_CATALOGS_URL)
  public ResponseEntity<?> findAll() {
    List<BookCatalog> listCatalogs = bookCatalogRepository.findAll();
    return new ResponseEntity<List<BookCatalog>>(listCatalogs, HttpStatus.OK);
  }

  /**
   * Adds the catalog.
   *
   * @param catalogRequest
   *          the catalog request
   * @return the response entity
   */
  @PostMapping(ApplicationConstants.ADD_CATALOGS_URL)
  public ResponseEntity<?> addCatalog(@RequestBody BookCatalog catalogRequest) {
    Optional<BookCatalog> boOptional = bookCatalogRepository.findById(catalogRequest.getIsbn().toUpperCase());
    if (boOptional.isPresent()) {
      return new ResponseEntity<String>(ApplicationConstants.BOOK_ISBN_FOUND, HttpStatus.OK);
    }
    bookCatalogRepository.save(catalogRequest);
    return new ResponseEntity<String>(ApplicationConstants.BOOK_SUCCESS_MSG, HttpStatus.OK);
  }
  
  /**
   * Gets the catalog.
   *
   * @param bookCode
   *          the book code
   * @return the catalog
   */
  @GetMapping(ApplicationConstants.FIND_BY_CODE_URL)
  public ResponseEntity<?> getCatalog(@PathVariable("bookCode") String bookCode) {
    Optional<BookCatalog> boOptional = bookCatalogRepository.findById(bookCode.toUpperCase());
    return new ResponseEntity<BookCatalog>(boOptional.orElse(new BookCatalog()), HttpStatus.OK);
  }
  
  /**
   * update the catalog.
   *
   * @param catalogRequest
   *          the catalog request
   * @return the response entity
   */
  @PutMapping(ApplicationConstants.UPDATE_CATALOGS_URL)
  public ResponseEntity<?> updateCatalog(@PathVariable("bookCode") String bookCode,
      @RequestBody BookCatalog catalogRequest) {
    Optional<BookCatalog> boOptional = bookCatalogRepository.findById(bookCode.toUpperCase());
    if (!boOptional.isPresent()) {
      return new ResponseEntity<String>(ApplicationConstants.BOOK_NOT_FOUND, HttpStatus.NOT_FOUND);
    }
    catalogRequest.setIsbn(bookCode);
    bookCatalogRepository.save(catalogRequest);
    return new ResponseEntity<String>(ApplicationConstants.BOOK_UPDATE_MSG, HttpStatus.OK);
  }

}
