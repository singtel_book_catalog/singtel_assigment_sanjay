package com.singtel.app.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.singtel.app.util.ApplicationConstants;

/**
 * The Class WelcomeController.
 */
@RestController
public class WelcomeController {

  /**
   * Home.
   *
   * @return the string
   */
  @GetMapping("/")
  public String home() {
    return ApplicationConstants.SERVICE_UP_MESSAGE;
  }
}