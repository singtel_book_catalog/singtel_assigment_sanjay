package com.singtel.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * The Class BookCatalogServiceApplication.
 */
@SpringBootApplication
public class BookCatalogServiceApplication {

  /**
   * The main method.
   *
   * @param args
   *          the arguments
   */
  public static void main(String[] args) {
    SpringApplication.run(BookCatalogServiceApplication.class, args);
  }

}
