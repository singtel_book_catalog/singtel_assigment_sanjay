package com.singtel.app.config;

import static springfox.documentation.builders.PathSelectors.regex;

import com.google.common.base.Predicate;
import com.singtel.app.util.ApplicationConstants;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * The Class SwaggerConfig.
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {

  /**
   * Posts api.
   *
   * @return the docket
   */
  @Bean
  public Docket postsApi() {
    return new Docket(DocumentationType.SWAGGER_2).groupName("public-api").apiInfo(apiInfo())
        .select().paths(postPaths()).build();
  }

  /**
   * Post paths.
   *
   * @return the predicate
   */
  private Predicate<String> postPaths() {
    return regex(ApplicationConstants.BOOK_SERVICE_URL + ".*");
  }

  /**
   * Api info.
   *
   * @return the api info
   */
  private ApiInfo apiInfo() {
    return new ApiInfoBuilder().title("BookCatalog API")
        .description("Swaager API reference for developers").version("1.0").build();
  }

}
