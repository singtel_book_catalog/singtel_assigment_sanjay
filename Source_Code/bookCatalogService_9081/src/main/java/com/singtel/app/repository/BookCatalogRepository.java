package com.singtel.app.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.singtel.app.model.BookCatalog;

/**
 * The Interface BookCatalogRepository.
 */
@Repository
public interface BookCatalogRepository extends JpaRepository<BookCatalog, String> {
  
  /**
   * Find by book codes.
   *
   * @param bookCodes the book codes
   * @return the list
   */
  @Query("select o from BookCatalog o where isbn in :bookCodes")
  public List<BookCatalog> findByBookCodes(@Param("bookCodes")List<String> bookCodes);

}
