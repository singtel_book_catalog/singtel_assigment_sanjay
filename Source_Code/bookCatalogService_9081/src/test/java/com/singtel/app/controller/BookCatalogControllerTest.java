package com.singtel.app.controller;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.singtel.app.controller.BookCatalogController;
import com.singtel.app.model.BookCatalog;
import com.singtel.app.repository.BookCatalogRepository;
import com.singtel.app.util.ApplicationConstants;

import java.util.List;
import java.util.Optional;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

/**
 * The Class UserAccountControllerTest.
 */
@RunWith(SpringRunner.class)
public class BookCatalogControllerTest {

  /** The mock mvc. */
  private MockMvc mockMvc;

  /** The book catalog controller. */
  @InjectMocks
  private BookCatalogController bookCatalogController;

  /** The book catalog repository. */
  @Mock
  private BookCatalogRepository bookCatalogRepository;

  /** The object mapper. */
  private ObjectMapper objectMapper;

  /** The book catalog json. */
  private String bookCatalogJson;

  /** The isbn. */
  private String isbn = "ISBN01";

  /** The cata logs list. */
  private List<BookCatalog> cataLogsList;

  /**
   * Inits the.
   *
   * @throws Exception
   *           the exception
   */
  @Before
  public void init() throws Exception {
    MockitoAnnotations.initMocks(this);
    mockMvc = MockMvcBuilders.standaloneSetup(bookCatalogController).build();

    // BuildMyString.com generated code. Please enjoy your string responsibly.

    bookCatalogJson = "[" + "  {" + "    \"title\": \"C ebook\"," + "    \"isbn\": \"ISBN01\","
        + "    \"desc\": \"C\"" + "  }," + "  {" + "    \"title\": \"C++ ebook\","
        + "    \"isbn\": \"ISBN02\"," + "    \"desc\": \"C++\"" + "  }," + "  {"
        + "    \"title\": \"JAVA ebook\"," + "    \"isbn\": \"ISBN03\"," + "    \"desc\": \"JAVA\""
        + "  }]";

    objectMapper = new ObjectMapper();
    cataLogsList = objectMapper.readValue(bookCatalogJson, new TypeReference<List<BookCatalog>>() {
    });

  }

  /**
   * Find all catalog test.
   *
   * @throws Exception
   *           the exception
   */
  @Test
  public void findAllCatalogTest() throws Exception {
    Mockito.when(bookCatalogRepository.findAll()).thenReturn(cataLogsList);
    final String findCatalogsUrl = ApplicationConstants.BOOK_SERVICE_URL
        + ApplicationConstants.FIND_CATALOGS_URL;
    MvcResult result = mockMvc.perform(get(findCatalogsUrl)).andExpect(status().isOk()).andReturn();

  }

  /**
   * Exist catalog test.
   *
   * @throws Exception
   *           the exception
   */
  @Test
  public void existCatalogTest() throws Exception {
    Optional<BookCatalog> optionalCatalog = Optional.of(cataLogsList.get(0));
    Mockito.when(bookCatalogRepository.findById(isbn)).thenReturn(optionalCatalog);

    final String addCatalogUrl = ApplicationConstants.BOOK_SERVICE_URL
        + ApplicationConstants.ADD_CATALOGS_URL;
    final String bookCatalogObjJson = objectMapper.writeValueAsString(optionalCatalog.get());
    MvcResult result = mockMvc
        .perform(
            post(addCatalogUrl).contentType(MediaType.APPLICATION_JSON).content(bookCatalogObjJson))
        .andExpect(status().isOk()).andReturn();
    assertEquals(result.getResponse().getContentAsString(), ApplicationConstants.BOOK_ISBN_FOUND);

  }

  /**
   * Creates the catalog test.
   *
   * @throws Exception
   *           the exception
   */
  @Test
  public void createCatalogTest() throws Exception {
    Optional<BookCatalog> optionalCatalog = Optional.empty();
    Mockito.when(bookCatalogRepository.findById(isbn.toLowerCase())).thenReturn(optionalCatalog);

    final String addCatalogUrl = ApplicationConstants.BOOK_SERVICE_URL
        + ApplicationConstants.ADD_CATALOGS_URL;
    final String bookCatalogObjJson = objectMapper.writeValueAsString(cataLogsList.get(0));
    MvcResult result = mockMvc
        .perform(
            post(addCatalogUrl).contentType(MediaType.APPLICATION_JSON).content(bookCatalogObjJson))
        .andExpect(status().isOk()).andReturn();
    assertEquals(result.getResponse().getContentAsString(), ApplicationConstants.BOOK_SUCCESS_MSG);
  }
  /**
   * Find  catalog test.
   *
   * @throws Exception
   *           the exception
   */
  @Test
  public void findCatalogTest() throws Exception {
    Optional<BookCatalog> optionalCatalog = Optional.of(cataLogsList.get(0));
    Mockito.when(bookCatalogRepository.findById(isbn)).thenReturn(optionalCatalog);
    final String findCatalogsUrl = ApplicationConstants.BOOK_SERVICE_URL
        + ApplicationConstants.FIND_BY_CODE_URL;
    MvcResult result = mockMvc.perform(get(findCatalogsUrl,isbn)).andExpect(status().isOk()).andReturn();
  }
  
  /**
   * Update catalog test.
   *
   * @throws Exception
   *           the exception
   */
  @Test
  public void updateCatalogExistTest() throws Exception {
    Optional<BookCatalog> optionalCatalog = Optional.of(cataLogsList.get(0));
    Mockito.when(bookCatalogRepository.findById(isbn)).thenReturn(optionalCatalog);

    final String updateCatalogUrl = ApplicationConstants.BOOK_SERVICE_URL
        + ApplicationConstants.UPDATE_CATALOGS_URL;
    final String bookCatalogObjJson = objectMapper.writeValueAsString(optionalCatalog.get());
    MvcResult result = mockMvc
        .perform(
            put(updateCatalogUrl,isbn,bookCatalogObjJson).contentType(MediaType.APPLICATION_JSON).content(bookCatalogObjJson))
        .andExpect(status().isOk()).andReturn();
    assertEquals(result.getResponse().getContentAsString(), ApplicationConstants.BOOK_UPDATE_MSG);

  }
  

  /**
   * Update catalog test.
   *
   * @throws Exception
   *           the exception
   */
  @Test
  public void updateCatalogNotExistTest() throws Exception {
    Optional<BookCatalog> optionalCatalog = Optional.of(new BookCatalog());
    Mockito.when(bookCatalogRepository.findById(isbn.toLowerCase())).thenReturn(optionalCatalog);

    final String updateCatalogUrl = ApplicationConstants.BOOK_SERVICE_URL
        + ApplicationConstants.UPDATE_CATALOGS_URL;
    final String bookCatalogObjJson = objectMapper.writeValueAsString(optionalCatalog.get());
    MvcResult result = mockMvc
        .perform(
            put(updateCatalogUrl,isbn,bookCatalogObjJson).contentType(MediaType.APPLICATION_JSON).content(bookCatalogObjJson))
        .andExpect(status().isNotFound()).andReturn();
    assertEquals(result.getResponse().getContentAsString(), ApplicationConstants.BOOK_NOT_FOUND);

  }
}