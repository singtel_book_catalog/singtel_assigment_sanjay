package com.singtel.app;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.singtel.app.BookCatalogServiceApplication;

/**
 * The Class BookCatalogServiceApplicationTests.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class BookCatalogServiceApplicationTests {

  /**
   * Context loads.
   */
  @Test
  public void contextLoads() {
    BookCatalogServiceApplication.main(new String[] {});
  }
}
