package com.singtel.app.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.singtel.app.controller.WelcomeController;
import com.singtel.app.util.ApplicationConstants;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

/**
 * The Class WelcomeControllerTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class WelcomeControllerTest {

  /** The mock mvc. */
  private MockMvc mockMvc;

  /** The welcome controller. */
  @InjectMocks
  private WelcomeController welcomeController;

  /**
   * Sets the up.
   *
   * @throws Exception
   *           the exception
   */
  @Before
  public void setUp() throws Exception {
    mockMvc = MockMvcBuilders.standaloneSetup(welcomeController).build();
  }

  /**
   * Home test.
   *
   * @throws Exception
   *           the exception
   */
  @Test
  public void homeTest() throws Exception {
    mockMvc.perform(get(ApplicationConstants.HOME_URL)).andExpect(status().isOk())
        .andExpect(content().string(ApplicationConstants.SERVICE_UP_MESSAGE));

  }

}
