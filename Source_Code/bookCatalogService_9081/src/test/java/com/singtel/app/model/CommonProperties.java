package com.singtel.app.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.agileware.test.PropertiesTester;
import org.junit.Before;
import org.junit.Test;

import com.singtel.app.model.BookCatalog;

// TODO: Auto-generated Javadoc
/**
 * The Class CommonProperties.
 */
public class CommonProperties {

  /** The book ctalog. */
  private BookCatalog bookCtalog;

  /**
   * Inits the.
   */
  @Before
  public void init() {
    bookCtalog = new BookCatalog();
  }

  /**
   * Test properties.
   *
   * @throws Exception
   *           the exception
   */
  @Test
  public void testProperties() throws Exception {

    PropertiesTester tester = new PropertiesTester();
    tester.testAll(BookCatalog.class);

    assertNotNull(BookCatalog.class.newInstance().toString());

    assertEquals(bookCtalog.hashCode(), bookCtalog.hashCode());
    BookCatalog bookCtalogRef = bookCtalog;
    assertEquals(bookCtalog.equals(bookCtalogRef), bookCtalogRef.equals(bookCtalog));
  }

}
